export default {
  select: {
    empty: 'Try another search.',
  },
  header: {
    coins: 'Coins',
    exchanges: 'Exchanges',
    charts: 'Charts',
    api: 'API',
  },
  pages: {
    index: {
      table: {
        rank: 'Rank',
        name: 'Name',
        price: 'Price',
        marketCap: 'Market Cap',
        vwap: 'VWAP (24Hr)',
        supply: 'Supply',
        volume24: 'Volume (24Hr)',
        change24: 'Change (24Hr)',
        trade: 'Trade',
      },
    },
  },
};
