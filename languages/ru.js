export default {
  select: {
    empty: 'Попробуйте другой запрос',
  },
  header: {
    coins: 'Монеты',
    exchanges: 'Обмены',
    charts: 'Графики',
    api: 'API',
  },
  pages: {
    index: {
      table: {
        rank: 'Рейтинг',
        name: 'Название',
        price: 'Цена',
        marketCap: 'Ограничение',
        vwap: 'VWAP (24 ч.)',
        supply: 'Поставка',
        volume24: 'Значение (24 ч.)',
        change24: 'Изменение (24 ч.)',
        trade: 'Торговать',
      },
    },
  },
};
