import ExampleWorker from '../worker/Service.worker';

function find(list, predicate) {
  for (let i = 0, len = list.length; i < len; i += 1) {
    if (predicate(list[i])) return list[i];
  }
  return null;
}

function remove(list, type, f) {
  list.splice(
    list.indexOf(
      find(list, (tuple) => tuple[0] === type && tuple[1] === f),
    ),
    1,
  );
}

export default function Worker() {
  /**
   * @type Worker
   */
  const worker = new ExampleWorker();
  const listeners = [];

  return Object.seal({
    inUse: false,
    listen(f) {
      listeners.push(['listen', f]);
      worker.addEventListener('message', f);
    },
    unlisten(f) {
      remove(listeners, 'listen', f);
      worker.removeEventListener('message', f);
    },
    error(f) {
      listeners.push(['error', f]);
      worker.addEventListener('messageerror', f);
    },
    unerror(f) {
      remove(listeners, 'error', f);
      worker.removeEventListener('messageerror', f);
    },
    async message({
      command,
      params,
    }, handler = null) {
      return new Promise((resolve) => {
        const channel = new MessageChannel();
        channel.port1.onmessage = (e) => {
          resolve(e.data);
        };

        const handlerChannel = new MessageChannel();
        if (handler) {
          handlerChannel.port1.onmessage = (e) => {
            handler(e.data);
          };
        }

        worker.postMessage(
          {
            command,
            params,
          },
          [channel.port2, handlerChannel.port2],
        );
      });
    },
    destroy() {
      listeners.forEach((tuple) => {
        this[`un${tuple[0]}`](tuple[1]);
      });
      worker.terminate();
    },
  });
}
