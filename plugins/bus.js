export default ({ store }) => {
  if (!process.browser) {
    return;
  }

  const WorkerHandler = require('../helpers/WorkerHandler').default;
  const worker = new WorkerHandler();

  store.sendMessage = (command, params, handler) => worker.message({
    command,
    params,
  }, handler);
};
