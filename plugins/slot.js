import Vue from 'vue';

Vue.use((VueInstance) => {
  VueInstance.mixin({
    methods: {
      slotExists(name) {
        return !!this.$slots[name] || !!this.$scopedSlots[name];
      },
    },
  });
});
