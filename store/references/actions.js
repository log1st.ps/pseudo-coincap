import { getAssets } from '../../apiRequests/assets/getAssets';
import { subscribeOnPrices as pricesSubscriber } from '../../apiRequests/prices/subscribeOnPrices';
import { getPrices } from '../../apiRequests/prices/getPrices';

export async function fetchAssets({ commit }, search) {
  const { sendMessage } = this;

  const { data } = await getAssets(sendMessage, search);

  commit('setAssets', data);
}

export async function fetchPrices({ commit }) {
  const { sendMessage } = this;

  const { data } = await getPrices(sendMessage);

  commit('setPrices', data);
}

export async function subscribeOnPrices({ commit, getters }) {
  const { sendMessage } = this;

  pricesSubscriber(sendMessage, {
    assets: getters.assets.map(({ id }) => id),
    handler: (assets) => {
      commit('updatePrices', assets);
    },
  });
}
