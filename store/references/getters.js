export const languages = ({ languages: value }) => value;

export const assets = ({ assets: value }) => value;

export const prices = ({ prices: value }) => value;
