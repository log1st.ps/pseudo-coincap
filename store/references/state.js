export default () => ({
  languages: [
    {
      key: 'en',
      text: 'English',
    },
    {
      key: 'ru',
      text: 'Русский',
    },
  ],
  assets: [],
  prices: [],
});
