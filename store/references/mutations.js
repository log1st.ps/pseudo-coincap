export const setAssets = (state, assets) => {
  state.assets = assets;
};

export const setPrices = (state, prices) => {
  state.prices = prices;
};

export const updatePrices = (state, prices) => {
  state.prices = state.prices.map((item) => ({
    ...item,
    priceUsd: item.id in prices ? prices[item.id] : item.priceUsd,
  }));
};
