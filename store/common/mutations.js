export const setAsset = (state, value) => {
  state.asset = value;
};

export const setLanguage = (state, value) => {
  state.language = value;
};

export const showSettings = (state) => {
  state.settingsVisible = true;
};

export const hideSettings = (state) => {
  state.settingsVisible = false;
};
