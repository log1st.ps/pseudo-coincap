export default () => ({
  navigation: [
    {
      key: 'coins',
    },
    {
      key: 'exchanges',
    },
    {
      key: 'charts',
    },
    {
      key: 'api',
    },
  ],
  asset: null,
  language: 'en',
  settingsVisible: false,
});
