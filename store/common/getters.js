export const navigation = ({ navigation: value }) => value;

export const language = ({ language: value }) => value;

export const asset = ({ asset: value }) => value;
