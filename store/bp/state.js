export default () => ({
  isMobile: false,
  isTablet: false,
  isDesktop: false,
  bp: 'desktop',
});
