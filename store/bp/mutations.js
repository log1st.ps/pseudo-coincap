export const handle = (state, width) => {
  state.isMobile = width <= 375;
  state.isTablet = width > 375 && width <= 768;
  state.isDesktop = width > 768;

  state.bp = state.isMobile ? 'mobile' : (state.isTablet ? 'tablet' : 'desktop');
};
