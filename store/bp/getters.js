export const isMobile = ({ isMobile: value }) => value;

export const isTablet = ({ isTablet: value }) => value;

export const isDesktop = ({ isDesktop: value }) => value;

export const bp = ({ bp: value }) => value;
