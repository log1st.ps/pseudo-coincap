export default {
  props: {
    value: [String, Number, Array, Object],
    placeholder: String,
    blurOnInput: Boolean,
    height: [Number, String],
    width: [Number, String],
    state: {
      type: String,
      default: 'pure',
    },
  },
  data() {
    return {
      isFocused: false,
      val: this.value,
    };
  },
  watch: {
    value(val) {
      this.val = val;
    },
    val(val) {
      this.setValue(val);
    },
  },
  methods: {
    async focus() {
      this.isFocused = true;

      await this.$nextTick;
      if (this.$refs.input) {
        this.$refs.input.focus();
      }
      this.$emit('focus');
    },
    async blur(e = null) {
      const { container } = this.$refs;

      if (e && (container === e.relatedTarget || container.contains(e.relatedTarget))) {
        return;
      }

      this.isFocused = false;

      await this.$nextTick;
      this.$emit('blur');
    },
    setValue(value) {
      this.$emit('input', value);

      if (this.blurOnInput) {
        this.blur();
      }
    },
  },
};
