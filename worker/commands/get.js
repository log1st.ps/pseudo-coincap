import axios from 'axios';

export default async ({ url, query }) => (await axios.get(url, {
  params: query,
})).data;
