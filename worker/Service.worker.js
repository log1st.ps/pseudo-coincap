self.addEventListener(
  'message',
  async ({ data: { command, params }, ports: [port, handler] }) => {
    command = require(`./commands/${command}`).default;
    const response = await command(params, handler);
    port.postMessage(response);
  },
);
