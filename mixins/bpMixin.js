import { mapGetters } from 'vuex';

export default {
  computed: {
    ...mapGetters({
      isMobile: 'bp/isMobile',
      isTablet: 'bp/isTablet',
      isDesktop: 'bp/isDesktop',
      bp: 'bp/bp',
    }),
  },
  methods: {
    computeClasses(block) {
      return ['isMobile', 'isTablet', 'isDesktop'].reduce((prev, key) => ({
        ...prev,
        [`${block}--${key}`]: this[key],
      }), {});
    },
  },
};
