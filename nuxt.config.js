import en from './languages/en';
import ru from './languages/ru';

module.exports = {
  head: {
    title: 'Pseudo Coincap',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'Pseudo Coincap test task' },
    ],
    link: [
      {
        href: 'https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap',
        rel: 'stylesheet',
      },
    ],
  },
  loading: { color: 'rgb(63, 81, 181)' },
  plugins: [
    '~plugins/bus',
    '~plugins/fragment',
    '~plugins/slot',
  ],
  modules: [
    'nuxt-vuex-localstorage',
    'nuxt-svg-loader',
    ['@nuxtjs/pwa', { icon: false }],
    [
      'nuxt-i18n',
      {
        strategy: 'no_prefix',
        locales: ['en'],
        defaultLocale: 'en',
        vueI18n: {
          fallbackLocale: 'en',
          messages: {
            en,
            ru,
          },
        },
      },
    ],
  ],
  buildModules: [
    '@nuxtjs/dotenv',
  ],
  build: {
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }

      config.output.globalObject = 'this';

      if (isClient) {
        config.module.rules.unshift({
          test: /\.worker\.js$/,
          loader: 'worker-loader',
          exclude: /(node_modules)/,
          options: {
            inline: true,
          },
        });
      }
    },
  },
};
