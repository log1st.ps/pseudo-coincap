import { apiUrl } from '../api';

export const getPrices = async (
  sendMessage,
) => sendMessage('get', {
  url: `${apiUrl}/assets`,
});
