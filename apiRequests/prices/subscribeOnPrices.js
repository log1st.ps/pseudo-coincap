import { wsUrl } from '../api';

export const subscribeOnPrices = (
  sendMessage,
  {
    assets,
    handler,
  },
) => sendMessage('subscribe', {
  url: `${wsUrl}/prices?assets=${assets.join(',')}`,
}, handler);
