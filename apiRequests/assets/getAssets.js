import { apiUrl } from '../api';

export const getAssets = async (
  sendMessage,
  query,
) => sendMessage('get', {
  url: `${apiUrl}/assets`,
  query,
});
