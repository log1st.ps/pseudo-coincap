const apiUrl = process.env.API_URL;
const wsUrl = process.env.WS_URL;

export {
  apiUrl,
  wsUrl,
};
